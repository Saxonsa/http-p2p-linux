#ifndef UDPSERVER_H
#define UDPSERVER_H

// linux socket
#include <netinet/in.h>

// head
#include "../common/PeerList/PeerList.h"

using namespace std;

static pthread_mutex_t peerListLock = PTHREAD_MUTEX_INITIALIZER;
class UDPServer
{
    private:
        // udp serve
        int server_fd;
        struct sockaddr_in udp_server_addr;
        int on;

        PeerList *peerList;
        char* request_source_name;

        void Error(const char*);
        int64 GetTickCount();
        static void* HandleClientRequest(void*);


    public:
        UDPServer() {
            this->on = 1;
            this->peerList = new PeerList();
            this->request_source_name = new char[MAX_USERNAME];
        }

        pthread_t handleClientRequestTid;
        void InitUDPServer();

        ~UDPServer() {
            delete peerList;
            delete [] request_source_name;
        }
};

#endif
