// standard lib
#include <iostream>

// head
#include "UDPServer.h"


using namespace std;

int main(int argc, char**argv)
{
    UDPServer* udpserver = new UDPServer();
    udpserver->InitUDPServer();
    pthread_join(udpserver->handleClientRequestTid, NULL);
    return 0;
}
