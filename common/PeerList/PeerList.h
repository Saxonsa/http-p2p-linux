#ifndef __MESSAGE_H__
#define __MESSAGE_H__

#include <arpa/inet.h>


#define MAX_PACKET_SIZE 1024 // max packet size for send and recv
#define MAX_USERNAME 15 // max length of user name allowed
#define MAX_TRY_NUMBER 5
#define MAX_ADDR_NUMBER 5
#define UDP_SERVER_PORT 4567 // udp server port
#define MAX_TEXT_LEN 100

typedef long long int int64;

// address structure
struct ADDR_INFO
{
    in_addr_t ip;
    in_port_t port;
};

// peer info
struct PEER_INFO
{
    char username[MAX_USERNAME];        // usernmae
    ADDR_INFO addr[MAX_ADDR_NUMBER];    // an array composed by private and public ip
    int addrNum;              
    ADDR_INFO p2pAddr;                  // addr for p2p communication (client)
    int64 LastActiveTime;       // note user active time (server)
};

struct Message
{
    int type;
    PEER_INFO peer;
    char text[MAX_TEXT_LEN];
};

// 用户直接与服务器之间发送的消息
#define USERLOGIN	101		    // 用户登陆服务器
#define USERLOGOUT	102		    // 用户登出服务器
#define USERLOGACK  103

#define GETPEERLIST	104		    // 请求用户列表
#define USERLISTCMP	105		    // 列表传输结束

#define USERACTIVEQUERY	106			// 服务器询问一个用户是否仍然存活
#define USERACTIVEQUERYACK	107		// 服务器询问应答

// 通过服务器中转，用户与用户之间发送的消息
#define REQUESTPUNCH	108			// 请求与一个用户建立连接
#define REQUESTPUNCHACK 109		    // 连接应答，此消息用于打洞

// 用户直接与用户之间发送的消息
#define P2PMESSAGE		110			// 发送消息
#define P2PMESSAGEACK	111			// 收到消息的应答


class PeerList
{
    public:
        PeerList() {
            this->totalSize = 100;
            this->currrentSize = 0;
            this->h_peerList = new PEER_INFO[this->totalSize];
        }

        ~PeerList() {
            delete[] h_peerList;
        }

        bool AddPeer(PEER_INFO* peer); // add a peer node into peer list
        PEER_INFO* GetPeer(char* username); // search specific peer node through username
        void DeletePeer(char* username); // delete a peer node from peer list through username
        void DeleteAllPeers(); // delete all nodes on peer list

        PEER_INFO* h_peerList; // head point of peer list
        int currrentSize; // size of peer list

    protected:
        int totalSize;
};


#endif // __MESSAGE_H__
