#ifndef AES_H
#define AES_H

#include <iostream>
#include <cryptopp/aes.h>
#include <cryptopp/default.h>
#include <cryptopp/filters.h>
#include <cryptopp/files.h>
#include <cryptopp/osrng.h>

#include <stdlib.h>
#include <string>

using namespace CryptoPP;
using namespace std;

class MyAES // mm
{
	public:
		byte key[ CryptoPP::AES::DEFAULT_KEYLENGTH ];
		byte iv[ CryptoPP::AES::BLOCKSIZE];

		MyAES() {};
		void initKV();
        string encrypt(string plainText);
		string decrypt(string cipherTextHex);
		~MyAES() {};
};

#endif
