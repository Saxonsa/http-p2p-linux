#ifndef PROXY_H
#define PROXY_H

// linux socket
#include <netinet/in.h>

#define LISTEN_PORT 5019
#define DEFAULT_PORT 80
#define MAX_SIZE 65536
#define MAX_HOST_NAME 512

struct Protocol{
    int webServer;
    int requestClient;
};

class Proxy
{   

    private:        
        struct sockaddr_in Server;
        int RecvHTTPRequest(int s, char* buf, int bufSize);
        int ConnectToServer(int &s, char* recvBuf, int len);
        int InitWebServerHost(int* s, char* HostName, int Port);
        int PreResponse(int client);
        int SendData(int s, const char* buf, int bufSize);
        int ExchangeData(void* peer);
    
    public:
        int serverSocket;
        struct sockaddr_in Client;
        socklen_t addr_len;

        void Error(const char* message);
        bool InitServerSocket();
        static void* serverThread(void* peer);
        static void* excThread(void* peer);
        static void* excThreadReverse(void* peer);
        void StartProxy();
};

struct Peer {
    Proxy* proxy;
    Protocol* protocol;
};

#endif
