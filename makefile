objects = proxy.o

proxy: $(objects)
	g++ -g -o proxy $(objects) -lpthread

proxy.o: proxy.h

.PHONY: clean
clean:
	rm proxy $(objects)