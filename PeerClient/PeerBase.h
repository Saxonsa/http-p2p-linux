#ifndef PEERBASE_H
#define PEERBASE_H

#include <unistd.h>
#include "../common/PeerList/PeerList.h"

class PeerBase
{
    protected:
        char* udpSend;
        char* udpRecv;
        char* tcpSend;
        char* tcpRecv;

    public:
        PeerBase() {
            this->udpSend = new char[MAX_PACKET_SIZE];
            this->udpRecv = new char[MAX_PACKET_SIZE];
            this->tcpSend = new char[MAX_PACKET_SIZE];
            this->tcpRecv = new char[MAX_PACKET_SIZE];
        }

        ~PeerBase() {
            delete [] udpSend;
            delete [] udpRecv;
            delete [] tcpSend;
            delete [] tcpRecv;
        }
};

#endif
